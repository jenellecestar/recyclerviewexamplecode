package com.example.robin.recylerviewexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements MyAdapter.ItemClickListener{


    private MyAdapter mAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        // data to populate the RecyclerView with
        ArrayList<String> movies = new ArrayList<>();
        movies.add("Black Panther");
        movies.add("Deadpool 2");
        movies.add("Avengers: Infinity War");
        movies.add("Go Goa Gone");
        movies.add("3 Idiots");


        // Add a divider bewteen each row



        // set up the RecyclerView
        RecyclerView recyclerView = findViewById(R.id.my_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        LinearLayoutManager mlm = (LinearLayoutManager) recyclerView.getLayoutManager();


        // add a "line" between each row
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
            mlm.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);


        mAdapter = new MyAdapter(this, movies);
        mAdapter.setClickListener(this);
        recyclerView.setAdapter(mAdapter);

    }


    // Click handler for when person clicks on a row
    @Override
    public void onItemClick(View view, int position) {
        Toast.makeText(this, "You clicked " + mAdapter.getItem(position) + " on row number " + position, Toast.LENGTH_SHORT).show();
    }

}
